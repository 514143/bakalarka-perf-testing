from pathlib import Path
import time

import mlflow
import numpy as np
import pandas
import PIL
import slidelip
from numpy.typing import NDArray
from torch.utils.data import DataLoader, Dataset
from tqdm import tqdm


class WsiDataset(Dataset):
    def __init__(self, tiles: pandas.DataFrame) -> None:
        self.tiles = tiles

    def __len__(self) -> int:
        return len(self.tiles)

    def __getitem__(self, index) -> NDArray[np.uint8]:
        sample = self.tiles.iloc[index]

        return extract_tile(
            slide_fp=Path(sample["slide_fp"]).resolve(),
            coord_x=sample["coord_x"],
            coord_y=sample["coord_y"],
            tile_size=sample["tile_size"],
            level=sample["sample_level"],
        )


def extract_tile(
    slide_fp: Path, coord_x: int, coord_y: int, tile_size: int, level: int
) -> NDArray:
    """Extracts a tile from a slide using the supplied coordinate values.

    Args:
        slide_fp (Path): Path to the slide.
        coord_x (int): Coordinates of a tile to be extracted at OpenSlide level 0 resolution.
        coord_y (int): Coordinates of a tile to be extracted at OpenSlide level 0 resolution.
        tile_size (int): Size of the tile to be extracted.
        level (int): Resolution level from which tile should be extracted.

    Returns:
        NDArray: RGB Tile represented as numpy array.
    """
    wsi = slidelip.open_slide(slide_path=slide_fp)
    bg_tile = PIL.Image.new(mode="RGB", size=(tile_size, tile_size), color="#FFFFFF")
    im_tile = wsi.read_region(
        location=(coord_x, coord_y), level=level, size=(tile_size, tile_size)
    )
    bg_tile.paste(im=im_tile, mask=im_tile, box=None)
    wsi.close()
    return np.array(bg_tile)


def mlflow_load_tiles(data_uris: list[str]) -> pandas.DataFrame:
    mlflow.set_tracking_uri("http://mlflow.rationai-mlflow:5000/")
    slides_dfs = []
    tiles_dfs = []
    for uri in data_uris:
        try:
            fp = Path(mlflow.artifacts.download_artifacts(artifact_uri=uri))
        except mlflow.MlflowException as e:
            raise FileNotFoundError(f"Cannot fetch data from {uri}.") from e

        try:
            slides = pandas.read_parquet(fp / "slides.parquet")
            tiles = pandas.read_parquet(fp / "tiles.parquet")
        except OSError as e:
            raise FileNotFoundError(f"Cannot load data from {fp}.") from e

        slides_dfs.append(slides)
        tiles_dfs.append(tiles)

    return pandas.concat(tiles_dfs).join(
        pandas.concat(slides_dfs).set_index("slide_name"), on="slide_name"
    )


def run_original(tiles, workers) -> float:
    data = WsiDataset(tiles=tiles)
    dataloader = DataLoader(
        data, batch_size=4, num_workers=workers, persistent_workers=True, shuffle=True
    )

    start = time.time()
    for batch in tqdm(dataloader, total=len(dataloader)):
        pass

    return time.time() - start
