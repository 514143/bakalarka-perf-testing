import logging
from pathlib import Path
import sys

import mlflow
import pandas

from original_test import run_original
from empaia_test import run_empaia


def mlflow_load_tiles(data_uris: list[str]) -> pandas.DataFrame:
    slides_dfs = []
    tiles_dfs = []
    for uri in data_uris:
        try:
            fp = Path(mlflow.artifacts.download_artifacts(artifact_uri=uri))
        except mlflow.MlflowException as e:
            raise FileNotFoundError(f"Cannot fetch data from {uri}.") from e

        try:
            slides = pandas.read_parquet(fp / "slides.parquet")
            tiles = pandas.read_parquet(fp / "tiles.parquet")
        except OSError as e:
            raise FileNotFoundError(f"Cannot load data from {fp}.") from e

        slides_dfs.append(slides)
        tiles_dfs.append(tiles)

    return pandas.concat(tiles_dfs).join(
        pandas.concat(slides_dfs).set_index("slide_name"), on="slide_name"
    )


if __name__ == "__main__":
    mlflow_uri = sys.argv[1]
    original_artifacts_uri = sys.argv[2]
    empaia_artifacts_uri = sys.argv[3]
    workers = int(sys.argv[4])

    mlflow.set_tracking_uri(mlflow_uri)

    tiles_empaia = mlflow_load_tiles([empaia_artifacts_uri])
    tiles_original = mlflow_load_tiles([original_artifacts_uri])

    run_empaia(tiles_empaia, workers)
    original_time = run_original(tiles_original, workers)

    logging.critical(
        f"Original: {original_time}"
    )  # FIXME It should be probably logged in a different way
