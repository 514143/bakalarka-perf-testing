import logging
from empaia.empaia_token_manager import EmpaiaTokenManager
from ray.train.torch import TorchTrainer
import time
import pandas
import io
from numpy.typing import NDArray
from ray.train.torch import prepare_data_loader
import PIL

import ray.train.lightning
import numpy as np
from torch.utils.data import DataLoader, Dataset
from tqdm import tqdm

from empaia.empaia_client import EmpaiaClient

TOKEN_MANAGER = None


def run_empaia(tiles, workers):
    init_token_manager(tiles)
    scaling_config = ray.train.ScalingConfig(num_workers=workers)

    trainer = TorchTrainer(
        train_func,
        scaling_config=scaling_config,
        train_loop_config={"tiles": tiles},
    )
    trainer.fit()


def init_token_manager(tiles: pandas.DataFrame):
    token_manager = EmpaiaTokenManager.options(
        name="token_manager", get_if_exists=True
    ).remote(
        client_id="WBC_CLIENT",
        token_url="https://auth-xopat.dyn.cloud.e-infra.cz/auth/realms/EMPAIA/protocol/openid-connect/token",
        auth_url="https://auth-xopat.dyn.cloud.e-infra.cz/auth/realms/EMPAIA/protocol/openid-connect/auth/device",
        workbench_base_url="https://workbench-xopat.dyn.cloud.e-infra.cz/v3",
        app_id="4e485b74-413e-477d-8e09-2c38ae57e582",
    )
    ray.get(token_manager.init.remote())

    cases = set()
    for i in range(len(tiles)):
        case_id = tiles.iloc[i]["slide_fp"].split("_")[0]
        cases.add(case_id)

    refs = []
    for case in cases:
        refs.append(token_manager.start_refreshing_scope_token_for_case.remote(case))

    ray.get(refs)

    global TOKEN_MANAGER
    TOKEN_MANAGER = token_manager


class WsiDatasetEmpaia(Dataset):
    def __init__(self, tiles: pandas.DataFrame) -> None:
        self.tiles = tiles
        token_manager = ray.get_actor("token_manager")
        # token_manager = TOKEN_MANAGER
        base_url = ray.get(token_manager.get_workbench_base_url.remote())
        self.empaia_client = EmpaiaClient(
            base_url=base_url, token_manager=token_manager
        )

    def __len__(self) -> int:
        return len(self.tiles)

    def __getitem__(self, index) -> NDArray[np.uint8]:
        sample = self.tiles.iloc[index]

        case_id, slide_id = sample["slide_fp"].split("_")

        tile = extract_tile_empaia(
            empaia_client=self.empaia_client,
            slide_id=slide_id,
            case_id=case_id,
            coord_x=sample["coord_x"],
            coord_y=sample["coord_y"],
            tile_size=sample["tile_size"],
            level=sample["sample_level"],
        )

        return tile


def extract_tile_empaia(
    empaia_client: EmpaiaClient,
    slide_id: str,
    case_id: str,
    coord_x: int,
    coord_y: int,
    tile_size: int,
    level: int,
) -> NDArray:
    """Extracts a tile from a slide using the supplied coordinate values.

    Args:
        slide_fp (Path): Path to the slide.
        coord_x (int): Coordinates of a tile to be extracted at OpenSlide level 0 resolution.
        coord_y (int): Coordinates of a tile to be extracted at OpenSlide level 0 resolution.
        tile_size (int): Size of the tile to be extracted.
        level (int): Resolution level from which tile should be extracted.

    Returns:
        NDArray: RGB Tile represented as numpy array.
    """
    bg_tile = PIL.Image.new(mode="RGB", size=(tile_size, tile_size), color="#FFFFFF")
    im_tile_bytes = empaia_client.get_region(
        slide_id, case_id, coord_x, coord_y, tile_size, tile_size, level
    )
    im_tile = PIL.Image.open(io.BytesIO(im_tile_bytes))

    bg_tile.paste(im=im_tile, box=None)
    return np.array(bg_tile)


def train_func(config):
    data_empaia = WsiDatasetEmpaia(tiles=config["tiles"])
    train_dataloader = prepare_data_loader(
        DataLoader(
            data_empaia,
            batch_size=4,
            shuffle=True,
        )
    )
    start = time.time()

    # NOTE: the total number that is displayed in the progress bar is number of batches per single worker, not number of all tiles
    for batch in tqdm(train_dataloader, total=len(train_dataloader)):
        pass

    logging.critical(
        f"Empaia: {time.time() - start}"
    )  # FIXME It should be probably logged in a different way
