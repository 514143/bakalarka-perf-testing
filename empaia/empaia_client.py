import base64
import json
import time
from typing import Any, cast

import ray
import requests

from empaia.empaia_token_manager import Case, Root


REQUEST_TIMEOUT = 10


class EmpaiaClient:
    def __init__(self, base_url: str, token_manager) -> None:
        self.__token_manager = token_manager
        self.__base_url = base_url
        self.__http_session = requests.Session()
        self.__cases: dict[str, Case] = {}
        self.__root: Root | None = None

    def get_tile(
        self,
        slide_id: str,
        level: int,
        x: int,
        y: int,
        image_format: str = "jpeg",
    ) -> bytes:
        params = {
            "image_format": image_format,
            "image_channels": [0, 1, 2],
        }
        binary_tile = self.__make_request(
            f"slides/{slide_id}/tile/level/{level}/tile/{x}/{y}",
            json_response=False,
            params=params,
        )

        return cast(bytes, binary_tile)

    def get_region(
        self,
        slide_id: str,
        case_id: str,
        x: int,
        y: int,
        width: int,
        height: int,
        level: int,
        image_format: str = "jpeg",
    ) -> bytes:
        params = {
            "image_format": image_format,
            "image_channels": [0, 1, 2],
        }
        binary_tile = self.__make_request_scope(
            case_id,
            f"slides/{slide_id}/region/level/{level}/start/{x}/{y}/size/{width}/{height}",
            json_response=False,
            params=params,
        )

        return cast(bytes, binary_tile)

    def __make_request_scope(
        self,
        case_id: str,
        endpoint: str,
        method: str = "get",
        data: dict[str, Any] | None = None,
        json_response: bool = True,
        params: dict[str, Any] | None = None,
    ) -> Any:
        if case_id not in self.__cases or time.time() > (
            self.__cases[case_id].exp - self.__cases[case_id].lifetime // 2
        ):
            self.__cases[case_id] = ray.get(
                self.__token_manager.get_scope_for_case.remote(case_id)
            )

        case = self.__cases[case_id]

        with (
            self.__http_session.request(
                method,
                f"{self.__base_url}/scopes/{case.scope_id}/{endpoint}",
                headers={"Authorization": f"Bearer {case.access_token}"},
                json=data,
                params=params,
            ) as res,
        ):
            res.raise_for_status()
            return res.json() if json_response else res.content

    def __make_request(
        self,
        endpoint: str,
        method: str = "get",
        data: dict[str, Any] | None = None,
        json_response: bool = True,
        params: dict[str, Any] | None = None,
    ) -> Any:
        if self.__root is None or time.time() > (
            self.__root.exp - self.root_.lifetime // 2
        ):
            self.__root = ray.get(self.__token_manager.get_root.remote())

        access_token = self.__root.access_token
        user_id = self.__extract_user_id_from_access_token(access_token)

        with (
            self.__http_session.request(
                method,
                f"{self.__base_url}/{endpoint}",
                headers={"Authorization": f"Bearer {access_token}", "user-id": user_id},
                json=data,
                params=params,
            ) as res,
        ):
            res.raise_for_status()
            return res.json() if json_response else res.content

    def __extract_user_id_from_access_token(self, token: str) -> Any:
        token = token.split(".")[1]
        token += "=" * (-len(token) % 4)
        decoded_bytes = base64.b64decode(token)

        latin1_string = decoded_bytes.decode("latin-1")

        return json.loads(latin1_string)["sub"]
