import math
from typing import Any


class EmpaiaSlide:
    def __init__(self, slide_info: dict[str, Any], case_id: str) -> None:
        self.level_count = slide_info["num_levels"]
        self.level_sizes = [
            (level["extent"]["x"], level["extent"]["y"])
            for level in slide_info["levels"]
        ]
        self.level_downsamples = [
            level["downsample_factor"] for level in slide_info["levels"]
        ]
        self.name = slide_info["id"]

        tile_size_x = slide_info["tile_extent"]["x"]
        tile_size_y = slide_info["tile_extent"]["y"]

        self.tile_size = (tile_size_x, tile_size_y)
        self.case_id = case_id

    def number_of_tiles(self, level: int) -> tuple[int, int]:
        level_sizes = self.level_sizes[level]
        return math.ceil(level_sizes[0] / self.tile_size[0]), math.ceil(
            level_sizes[1] / self.tile_size[1]
        )
