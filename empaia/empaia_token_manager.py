import asyncio
import base64
import json
import logging
import os
import time
from typing import Any, cast

import aiohttp
import ray
import requests
from attr import dataclass


@dataclass
class Case:
    exam_id: str
    access_token: str
    scope_id: str
    exp: int
    lifetime: int
    initialization_event: asyncio.Event


@dataclass
class Root:
    access_token: str
    exp: int
    lifetime: int
    refresh_token: str


@dataclass
class Scope:
    id: str
    access_token: str


@ray.remote
class EmpaiaTokenManager:
    def __init__(
        self,
        client_id: str,
        token_url: str,
        auth_url: str,
        workbench_base_url: str,
        app_id: str,
    ) -> None:
        self.__client_id = client_id
        self.__token_url = token_url
        self.__workbench_base_url = workbench_base_url
        self.__auth_url = auth_url
        self.__app_id = app_id

        self.__http_session = None
        self.__cases: dict[str, Case] = {}
        self.__tasks: dict[str, asyncio.Task] = {}

    def init(self) -> None:
        refresh_token = None
        access_token = None

        refresh_token_from_env = os.environ.get("EMPAIA_REFRESH_TOKEN")
        if refresh_token_from_env:
            try:
                res = requests.post(
                    self.__token_url,
                    data={
                        "client_id": self.__client_id,
                        "refresh_token": refresh_token_from_env,
                        "grant_type": "refresh_token",
                    },
                )
                res.raise_for_status()

                refresh_token = cast(str, res.json()["refresh_token"])
                access_token = cast(str, res.json()["access_token"])
            except Exception:
                pass

        if not refresh_token:
            data = {
                "client_id": self.__client_id,
            }

            res = requests.post(self.__auth_url, data=data)

            logging.critical(
                f"AUTHENTICATION URL: {res.json()['verification_uri_complete']}"
            )

            data = {
                "grant_type": "urn:ietf:params:oauth:grant-type:device_code",
                "client_id": self.__client_id,
                "device_code": res.json()["device_code"],
            }

            response = requests.post(
                self.__token_url,
                data=data,
            )

            sleep_time = 5

            while response.status_code > 299:
                if response.json()["error"] != "authorization_pending":
                    raise Exception(response.json()["error_description"])

                time.sleep(sleep_time)

                response = requests.post(self.__token_url, data=data)

                if response.json().get("error") == "slow_down":
                    time.sleep(sleep_time)

            refresh_token = cast(str, response.json()["refresh_token"])
            access_token = cast(str, response.json()["access_token"])

        self.__root = Root(
            access_token=access_token,
            exp=self.__get_exp_from_token(access_token),
            lifetime=self.__get_lifetime_from_token(access_token),
            refresh_token=refresh_token,
        )

        self.__tasks["root"] = asyncio.create_task(
            self.__start_refreshing_root_tokens()
        )

    async def get_root(self) -> Root:
        if not self.__root.access_token:
            raise Exception(
                "User has not been authenticated yet! Please call `init` method first."
            )

        return self.__root

    async def start_refreshing_scope_token_for_case(self, case_id: str) -> None:
        if case_id in self.__cases:
            return

        event = asyncio.Event()
        self.__cases[case_id] = Case("", "", "", 0, 0, event)
        await self.__refresh_scope_token_for_case(case_id)
        event.set()

        self.__tasks[case_id] = asyncio.create_task(
            self.__start_refreshing_scope_token_for_case(case_id)
        )

    async def get_scope_for_case(self, case_id: str) -> Case:
        if case_id not in self.__cases:
            raise Exception(
                "Case token refreshing has not been started yet! Please call start_refreshing_scope_token_for_case first."
            )

        case_init_event = self.__cases[case_id].initialization_event
        if not case_init_event.is_set():
            await case_init_event.wait()

        print("RETURNING CASE")
        return self.__cases[case_id]

    def get_workbench_base_url(self) -> str:
        return self.__workbench_base_url

    ############################################################################

    async def __start_refreshing_scope_token_for_case(self, case_id: str) -> None:
        while True:
            await self.__refresh_scope_token_for_case(case_id)
            await asyncio.sleep(self.__cases[case_id].lifetime // 2)

    async def __start_refreshing_root_tokens(self) -> None:
        while True:
            await self.__refresh_root_tokens()
            await asyncio.sleep(self.__root.lifetime // 2)

    async def __make_request(
        self,
        endpoint: str,
        method: str = "get",
        json: dict[str, Any] | None = None,
    ) -> Any:
        access_token = (await self.get_root()).access_token
        user_id = self.__parse_jwt_token(access_token.split(".")[1])["sub"]
        async with (
            self.__get_http_session().request(
                method,
                f"{self.__workbench_base_url}/{endpoint}",
                headers={
                    "Authorization": f"Bearer {access_token}",
                    "user-id": user_id,
                },
                json=json,
            ) as res,
        ):
            res.raise_for_status()

            return await res.json()

    def __get_exp_from_token(self, token: str) -> int:
        parsed_access_token = self.__parse_jwt_token(token.split(".")[1])
        return cast(int, parsed_access_token["exp"])

    def __get_lifetime_from_token(self, token: str) -> int:
        parsed_access_token = self.__parse_jwt_token(token.split(".")[1])
        exp_time = cast(int, parsed_access_token["exp"])
        return exp_time - int(time.time())

    async def __refresh_scope_token_for_case(self, case_id: str) -> None:
        initialization_event = self.__cases[case_id].initialization_event

        if not initialization_event.is_set():
            data = {"case_id": case_id, "app_id": self.__app_id}
            res = await self.__make_request("examinations", method="put", json=data)
            exam_id = cast(str, res["id"])
            self.__cases[case_id].exam_id = exam_id

        result = await self.__make_request(
            f"examinations/{self.__cases[case_id].exam_id}/scope", method="put"
        )

        self.__cases[case_id].exp = self.__get_exp_from_token(result["access_token"])
        self.__cases[case_id].access_token = cast(str, result["access_token"])
        self.__cases[case_id].scope_id = cast(str, result["scope_id"])
        self.__cases[case_id].lifetime = self.__get_lifetime_from_token(
            result["access_token"]
        )

    async def __refresh_root_tokens(self) -> None:
        data = {
            "client_id": self.__client_id,
            "refresh_token": self.__root.refresh_token,
            "grant_type": "refresh_token",
        }

        async with self.__get_http_session().post(self.__token_url, data=data) as res:
            res.raise_for_status()
            result = await res.json()

        self.__root = Root(
            access_token=result["access_token"],
            exp=self.__get_exp_from_token(result["access_token"]),
            lifetime=self.__get_lifetime_from_token(result["access_token"]),
            refresh_token=result["refresh_token"],
        )

    def __parse_jwt_token(self, token: str) -> Any:
        token += "=" * (-len(token) % 4)
        decoded_bytes = base64.b64decode(token)

        latin1_string = decoded_bytes.decode("latin-1")

        return json.loads(latin1_string)

    def __get_http_session(self) -> aiohttp.ClientSession:
        if self.__http_session is None:
            self.__http_session = aiohttp.ClientSession()

        return self.__http_session

    @staticmethod
    def reset() -> None:
        EmpaiaTokenManager.__instantiated = False

    def reset_cases(self) -> None:
        self.__cases = {}
